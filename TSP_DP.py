import time

def tsp_rec_solve(d):
    def rec_tsp_solve(c, ts):
        assert c not in ts
        if ts:
            return min((d[lc][c] + rec_tsp_solve(lc, ts - set([lc]))[0], lc) for lc in ts)
        else:
            return (d[0][c], 0)

    best_tour = []
    c = 0
    cs = set(range(1, len(d)))
    while True:
        l, lc = rec_tsp_solve(c, cs)
        if lc == 0:
            break
        best_tour.append(lc)
        c = lc
        cs = cs - set([lc])

    best_tour = tuple(reversed(best_tour))

    return best_tour

start_time = time.time()

if __name__ == "__main__":

	# matrix representation of graph
	graph = [
		[0, 1685, 3753, 4633, 9089, 2335, 5631, 1475, 1729, 2785],
		[1395, 0, 3072, 5001, 7683, 2711, 5999, 2676, 2105, 2103],
		[5705, 4492, 0, 7543, 5212, 4850, 7405, 6779, 4244, 1568],
		[4187, 5386, 7813, 0, 9840, 3932, 3024, 2917, 4199, 6844],
		[12756, 11543, 5471, 10420, 0, 8354, 9065, 13830, 5872, 7039],
		[4378, 5376, 5562, 4224, 6753, 0, 2346, 3456, 607, 4593],
		[3489, 4688, 7115, 3335, 8236, 2803, 0, 2567, 3501, 6146],
		[1524, 2694, 4762, 2807, 9507, 2367, 3757, 0, 3347, 3794],
		[2235, 5027, 5213, 4133, 7361, 607, 2953, 2394, 0, 4244],
		[2467, 3438, 1725, 6243, 7143, 3550, 6105, 3799, 2944, 0]
	]
	print(tsp_rec_solve(graph))

print("--- %s seconds ---" % (time.time() - start_time))
