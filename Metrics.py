from enum import Enum

class Metrics(str, Enum):
    distance = "distance"
    duration = "duration"