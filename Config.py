from enum import Enum

class Config(int, Enum):
	NUM_VEHICLES = 1
	DEFAULT_DEPOT = 0
	DEFAULT_SERVICE_TIME = 900 # 900 sec = 15min.
	DRIVER_MAX_WORK_TIME = 57600 # maximmum work time for the driver (set into 16 hours). 
	WAITING_TIME = 900 # allow waitng time (900 sec = 15min.)
	PENALTY = 72000000 # allow waitng time (900 sec = 15min.)