from typing import List
from fastapi import Body, FastAPI
from pydantic import BaseModel

from TSP import TSP
from Metrics import Metrics
import time

class Matrix(BaseModel):
    distances: List[List[float]] 
    durations: List[List[float]]
    time_windows: List[List[int]]

app = FastAPI()

@app.get("/")
async def root():
    return {"message": "Hello World"}

@app.post("/solve/{metrics}")
def solver(metrics: Metrics, matrix: Matrix):

    start_time = time.time()
    tsp = TSP(metrics, matrix.distances, matrix.durations, matrix.time_windows)
    res = tsp.solve()
    print("--- %s seconds ---" % (time.time() - start_time))
    return res
