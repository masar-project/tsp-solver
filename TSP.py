from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp
from Metrics import Metrics
from Config import Config

def create_data_model(distance_matrix, duration_matrix, time_windows):
	# Stores the data for the problem
	data = {}
	data['distance_matrix'] = distance_matrix
	data['duration_matrix'] = duration_matrix
	data['time_windows'] = time_windows
	data['num_vehicles'] = Config.NUM_VEHICLES
	data['depot'] = Config.DEFAULT_DEPOT
	return data

class TSP:

	def print_solution(self, solution):
		"""Prints solution on console."""
		print('Objective: ', int(solution.ObjectiveValue()),' meters')

		# Display dropped nodes.
		dropped_nodes = 'Dropped nodes:'
		for node in range(self.__routing.Size()):
			if self.__routing.IsStart(node) or self.__routing.IsEnd(node):
				continue
			if solution.Value(self.__routing.NextVar(node)) == node:
				dropped_nodes += ' {}'.format(self.__manager.IndexToNode(node) + 1)
		print(dropped_nodes)

		# in our example we have only one car
		vehicle_id = 0

		# time 
		time_dimension = self.__routing.GetDimensionOrDie('Time')

		# start index
		index = self.__routing.Start(vehicle_id)

		# output
		plan_output = ''

		# total distance
		route_distance = 0

		while not self.__routing.IsEnd(index):
			
			# get cumulative travel time until this point
			time_var = time_dimension.CumulVar(index)

			# print min, max point to reach this point
			plan_output += '{0} Time({1},{2}) -> '.format( self.__manager.IndexToNode(index)+ 1, solution.Min(time_var), solution.Max(time_var))

			# save current point before changing it
			previous_index = index
			
			# get next node
			index = solution.Value(self.__routing.NextVar(index))

			# add the distance between old and new node
			route_distance += int(self.__distance_callback(previous_index, index))

		# get final travel time
		time_var = time_dimension.CumulVar(index)

		# add it to print
		plan_output += '{0} Time({1},{2})\n'.format(self.__manager.IndexToNode(index) + 1, solution.Min(time_var), solution.Max(time_var))

		# print total var  
		plan_output += 'Route Duration: {} min\n'.format(solution.Min(time_var))

		# print total distance
		plan_output += 'Route distance: {} meters\n'.format(int(route_distance))

		print(plan_output)

	def result(self, solution):

		"""Prints solution on console."""

		res = {
			"route_distance": 0,
			"route_duration": 0,
			"dropped_nodes": [],
			"locations": [],
		}

		# get dropped nodes
		for node in range(self.__routing.Size()):
			# check if node is visited
			if self.__routing.IsStart(node) or self.__routing.IsEnd(node):
				continue
			# check if node is propped
			if solution.Value(self.__routing.NextVar(node)) == node:
				res["dropped_nodes"].append(self.__manager.IndexToNode(node))

		index = self.__routing.Start(0)

		while not self.__routing.IsEnd(index):
			# get the current node
			res['locations'].append(self.__manager.IndexToNode(index))

			# save it as previous
			previous_index = index
			
			# get next node
			index = solution.Value(self.__routing.NextVar(index))

			# add the distance between old and new node
			res['route_distance'] += self.__distance_callback(previous_index, index)

			# add the duration between old and new node
			res['route_duration'] += self.__duration_callback(previous_index, index)

		# add the last node
		res['locations'].append(self.__manager.IndexToNode(index))
		res['route_distance'] = int(res['route_distance'])
		res['route_duration'] = int(res['route_duration'])
		res['original'] = solution.ObjectiveValue()
		
		return res

    # To use the routing solver, you need to create a distance (or transit) callback: 
    # a function that takes any pair of locations and returns the distance between them. 
    # The easiest way to do this is using the distance matrix.
	def __distance_callback(self, from_index, to_index):
		"""Returns the distance between the two nodes."""
		# Convert from routing variable Index to distance matrix NodeIndex.
		from_node = self.__manager.IndexToNode(from_index)
		to_node = self.__manager.IndexToNode(to_index)
		return self.__data['distance_matrix'][from_node][to_node]

	def __duration_callback(self, from_index, to_index):
		"""Returns the distance between the two nodes."""
		# Convert from routing variable Index to distance matrix NodeIndex.
		from_node = self.__manager.IndexToNode(from_index)
		to_node = self.__manager.IndexToNode(to_index)
		return self.__data['duration_matrix'][from_node][to_node] + self.__service_time

	# default constructor
	# params:
		# metric type  (distance: shortest distance, duration: shortest duration)
		# distance_matrix: matrix of distance between each node (in meters)
		# duration_matrix: matrix of duration time between each node (in seconds)
		# time windows:   (for each node, we define availability time)
		# service_time:   how much time the driver need for each node (in seconds, default: 900sec or 15min.) 
	def __init__(self, metrics: Metrics, distance_matrix, duration_matrix, time_windows, service_time = Config.DEFAULT_SERVICE_TIME):

		# define metrics type (distance, duration)
		self.__metrics = metrics
		self.__service_time = service_time

		# Instantiate the data problem.
		self.__data = create_data_model(distance_matrix, duration_matrix, time_windows)

		# Create the routing index manager.
		# inputs:
			# The number of rows of the distance matrix, which is the number of locations (including the depot)
			# The number of vehicles in the problem
			#The node corresponding to the depot.

		self.__manager = pywrapcp.RoutingIndexManager(len(self.__data['distance_matrix']), self.__data['num_vehicles'], self.__data['depot'])

		# Create Routing Model.
		self.__routing = pywrapcp.RoutingModel(self.__manager)

	# solve the problem with minimal duration
	def solve(self):
		# register callback function in routing class
		if self.__metrics == Metrics.duration:
			transit_callback_index = self.__routing.RegisterTransitCallback(self.__duration_callback)
		else:
			transit_callback_index = self.__routing.RegisterTransitCallback(self.__distance_callback)
	
		# Define cost of each arc.
		# The arc cost evaluator tells the solver how to calculate the cost of travel between any two locations
		# In this example, the arc cost evaluator is the transit_callback_index
		self.__routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

		# Add Time Windows constraint.
		self.__routing.AddDimension(
			transit_callback_index,
			Config.WAITING_TIME,  # allow waiting time
			Config.DRIVER_MAX_WORK_TIME,  # maximum time per vehicle
			False,  # Don't force start cumul to zero.
			'Time')
		time_dimension = self.__routing.GetDimensionOrDie('Time')

		# Add time window constraints for each location except depot.
		for location_idx, time_window in enumerate(self.__data['time_windows']):
			if location_idx == self.__data['depot']:
				continue
			index = self.__manager.NodeToIndex(location_idx)
			time_dimension.CumulVar(index).SetRange(time_window[0], time_window[1])

		# Add time window constraints for each vehicle start node.
		depot_idx = self.__data['depot']
		vehicle_id = 0
		index = self.__routing.Start(vehicle_id)
		time_dimension.CumulVar(index).SetRange(self.__data['time_windows'][depot_idx][0],self.__data['time_windows'][depot_idx][1])

		# Instantiate route start and end times to produce feasible times.
		self.__routing.AddVariableMinimizedByFinalizer(
			time_dimension.CumulVar(self.__routing.Start(vehicle_id)))
		self.__routing.AddVariableMinimizedByFinalizer(
			time_dimension.CumulVar(self.__routing.End(vehicle_id)))

		# Allow to drop nodes.
		penalty = Config.PENALTY
		for node in range(1, len(self.__data['distance_matrix'])):
			self.__routing.AddDisjunction([self.__manager.NodeToIndex(node)], penalty)

		# Solve the problem.
		solution = self.__routing.Solve()

		# Print solution on console.
		if solution:
			self.print_solution(solution)
			return self.result(solution)