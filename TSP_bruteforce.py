from sys import maxsize
import time
from itertools import permutations
V = 13

# implementation of traveling Salesman Problem
def travellingSalesmanProblem(graph, s):

	# store all vertex apart from source vertex
	vertex = []
	for i in range(V):
		if i != s:
			vertex.append(i)

	# store minimum weight Hamiltonian Cycle
	min_path = maxsize
	next_permutation=permutations(vertex)
	for i in next_permutation:

		# store current Path weight(cost)
		current_pathweight = 0

		# compute current path weight
		k = s
		for j in i:
			current_pathweight += graph[k][j]
			k = j
		current_pathweight += graph[k][s]

		# update minimum
		min_path = min(min_path, current_pathweight)
		
	return min_path

start_time = time.time()

if __name__ == "__main__":

	# matrix representation of graph
	graph = [
		[6717 , 8750 , 5917 , 2594 ,10274 ,12119 ,13130 , 3949 , 2370 , 2644 , 7382 ,12090 ,14275],
		[9839 , 2484 , 5635 ,10042 , 2152 ,13449 ,12792 ,14348 , 5616 ,10995 ,10120 ,10733 , 8075],
		[8946 , 7810 , 1094 , 1507 , 9246 , 6670 , 3296 ,13786 ,11274 , 6504 ,13531 , 7965 , 2264],
		[7328 , 5588 ,12140 ,11550 , 5990 , 2521 , 1984 , 9278 ,10669 , 5894 , 8705 , 9292 , 2671],
		[11612 ,13783 ,10214 ,11184 , 7231 , 9454 , 8352 , 4855 , 7598 ,11529 , 3210 , 3979 , 6671],
		[3467 ,14663 , 4763 ,12113 , 4927 , 6032 , 1990 , 4246 , 4263 , 6633 , 6147 , 5862 , 4491],
		[2601 , 1442 ,11764 ,14695 , 1966 , 1681 , 2246 , 2285 , 4753 , 3506 , 2269 ,10422 , 1425],
		[10674 ,13974 ,11720 ,13178 ,13637 , 8417 , 3037 , 7305 , 4905 ,10704 , 4479 , 9162 ,14771],
		[2828 ,10210 , 6660 ,12312 , 7240 , 1241 , 3433 , 9077 , 5829 , 6198 ,12627 ,10641 , 7719],
		[8587 , 1761 , 4635 , 9265 , 3830 ,10550 ,10721 , 5650 ,13277 ,13954 , 8277 , 5433 ,13830],
		[8889 , 7487 , 1632 , 8562 ,10639 , 2505 , 3472 , 5672 ,12038 , 6310 ,12620 , 8470 ,13866],
		[1138 ,14820 , 4516 ,12313 , 1515 ,14950 , 5310 ,10396 ,13952 , 7025 , 2348 , 6391 ,14938],
		[5566 , 8798 ,12293 ,14458 ,14231 ,14843 ,14301 , 5650 , 6030 ,12603 ,12147 , 9889 , 4043]];

	s = 0
	print(travellingSalesmanProblem(graph, s))

print("--- %s seconds ---" % (time.time() - start_time))
